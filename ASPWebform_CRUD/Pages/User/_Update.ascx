﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_Update.ascx.cs" Inherits="ASPWebform_CRUD.Pages.User._Update" %>

<form method="post" id="myForm">
    <div class="row">
        <div class="col-md-12 form-group">
            <label>Fisrt Name</label>
            <input type="text" class="form-control" name="FirstName" id="FirstName" value="<%= HttpContext.Current.Session["FirstName"].ToString() %>" />
        </div>
        <div class="col-md-12 form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" name="LastName" id="LastName" value="<%= HttpContext.Current.Session["LastName"].ToString() %>"  />
        </div>
        <div class="col-md-12 form-group">
            <input type="hidden" class="form-control" name="LastName" id="Id" value="<%= HttpContext.Current.Session["Id"].ToString() %>"  />
            <button type="button" class="btn btn-primary" id="demo">Update</button>
            <a href="#" class="btn btn-default" onclick="_Create()">بازگشت</a>
        </div>
    </div>
</form>

<script>

    $("#demo").click(function () {

        var data = "{FirstName: '" + $("#FirstName").val() + "'" +
                   ",LastName: '" + $("#LastName").val() + "'" +
                   ",Id: '" + $("#Id").val() + "'}";
        $.ajax({
            enctype: 'multipart/form-data',
            url: "Default.aspx/Update",
            data: data,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                _Read();
            }
        });
    });


</script>