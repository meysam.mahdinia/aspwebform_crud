﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPWebform_CRUD.Models.Database.Entities
{
    public class Product
    {
        private int _ProductID = 0;

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }

        private string _Name = string.Empty;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _Unit = string.Empty;

        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        private decimal _Qty = 0;

        public decimal Qty
        {
            get { return _Qty; }
            set { _Qty = value; }
        }
    }
}